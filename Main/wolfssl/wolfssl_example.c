#define __DEBUG__ 4
#ifndef __MODULE__
    #define __MODULE__ "httpclient_socket"
#endif
#include "debug/debug.h"
#include "debug/debug_console.h"

#include "lwip/sys.h"
#include "lwip/sockets.h"
#include "lwip/netdb.h"
#include "string.h"
#include "httpclient-socket.h"
#include "cmsis_os.h"
#include <stdio.h>
#include "target.h"

#ifdef HAVE_CONFIG_H
    #include <config.h>
#endif

#include <wolfssl/wolfcrypt/settings.h>
#include <wolfssl/ssl.h>
#include <wolfcrypt/test/test.h>
#include <wolfcrypt/benchmark/benchmark.h>


/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/
typedef struct func_args {
    int    argc;
    char** argv;
    int    return_code;
} func_args;

const char menu1[] = "\r\n"
    "\tt. WolfSSL Test\r\n"
    "\tb. WolfSSL Benchmark\r\n";


/*****************************************************************************
 * Public functions
 ****************************************************************************/
void wolfCryptDemo(void const * argument)
{
    uint8_t buffer[1] = {'t'};
    func_args args;

    memset(&args, 0, sizeof(args));
    DEBUG("\n1. Crypt Test\n");
    wolfcrypt_test(&args);
    DEBUG("Crypt Test: Return code %d\n", args.return_code);

    memset(&args, 0, sizeof(args));
    DEBUG("\n2. Benchmark Test\n");
    benchmark_test(&args);
    DEBUG("Benchmark Test: Return code %d\n", args.return_code);
}
