/*
	User setting for wolfssl

	HieuNT
	22/8/2017
*/

#ifndef __USER_SETTING_FOR_WOLFSSL_H_
#define __USER_SETTING_FOR_WOLFSSL_H_

#define FREERTOS

#define WOLFSSL_LWIP
#define HAVE_LWIP_NATIVE

#define WOLFSSL_STM32F2
#define WOLFSSL_STM32_CUBEMX

#define WOLFSSL_LOW_MEMORY

#define WOLFSSL_SHA512

#endif
