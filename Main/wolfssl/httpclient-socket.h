/*
  HTTP client using socket 

  HieuNT
*/

#ifndef __HTTP_CLIENT_SOCKET_H_
#define __HTTP_CLIENT_SOCKET_H_

#ifdef USE_DHCP
	#define IP_ADDR0  0
	#define IP_ADDR1  0
	#define IP_ADDR2  0
	#define IP_ADDR3  0

	#define GW_ADDR0  0
	#define GW_ADDR1  0
	#define GW_ADDR2  0
	#define GW_ADDR3  0

	#define MASK_ADDR0  0
	#define MASK_ADDR1  0
	#define MASK_ADDR2  0
	#define MASK_ADDR3  0
#else
	#define IP_ADDR0  10
	#define IP_ADDR1  157
	#define IP_ADDR2  9
	#define IP_ADDR3  133

	#define GW_ADDR0  10
	#define GW_ADDR1  157
	#define GW_ADDR2  9
	#define GW_ADDR3  254

	#define MASK_ADDR0  255
	#define MASK_ADDR1  255
	#define MASK_ADDR2  254
	#define MASK_ADDR3  0
#endif /* USE_DHCP */

#define SERVER_PORT "443"
#define SERVER_NAME "mrvina.vn"
#define GET_REQUEST "GET / HTTP/1.0\r\n\r\n"

#define SENDER_PORT_NUM 6000 
// #define SENDER_IP_ADDR "192.136.23.20"

#define SERVER_PORT_NUM 443
#define SERVER_IP_ADDRESS "163.44.207.218" // "192.168.1.10"

/* Includes ------------------------------------------------------------------*/
/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
int http_client_socket_get(const char *host, const char *path);
void http_client_test_dns(void);

#endif /* __HTTP_CLIENT_SOCKET_H_ */
