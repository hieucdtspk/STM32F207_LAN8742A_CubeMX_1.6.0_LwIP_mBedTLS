/*
	ssl client application header file

	HieuNT
	dd/mm/2017
*/

#ifndef __SSL_CLIENT_APP_H_
#define __SSL_CLIENT_APP_H_


#include "stm32f2xx_hal.h"
#include "cmsis_os.h"

#define SERVER_PORT "4433"
#define SERVER_NAME "192.168.1.10" 

#define GET_REQUEST "GET / HTTP/1.0\r\n\r\n"


#ifdef USE_DHCP

#define IP_ADDR0  0
#define IP_ADDR1  0
#define IP_ADDR2  0
#define IP_ADDR3  0

#define GW_ADDR0  0
#define GW_ADDR1  0
#define GW_ADDR2  0
#define GW_ADDR3  0

#define MASK_ADDR0  0
#define MASK_ADDR1  0
#define MASK_ADDR2  0
#define MASK_ADDR3  0

#else

#define IP_ADDR0  10
#define IP_ADDR1  157
#define IP_ADDR2  9
#define IP_ADDR3  133

#define GW_ADDR0  10
#define GW_ADDR1  157
#define GW_ADDR2  9
#define GW_ADDR3  254

#define MASK_ADDR0  255
#define MASK_ADDR1  255
#define MASK_ADDR2  254
#define MASK_ADDR3  0

#endif /* USE_DHCP */
#ifdef MBEDTLS_MEMORY_BUFFER_ALLOC_C
#define MAX_MEM_SIZE 35 * 1024
#endif

/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/

void SSL_Client(void const *argument);

#endif
