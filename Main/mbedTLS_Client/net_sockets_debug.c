
/*
	net_sockets debug info

	HieuNT
	dd/mm/2017
*/

#define __DEBUG__ 4
#ifndef __MODULE__
	#define __MODULE__ "net_socket_debug"
#endif
#include "debug/debug.h"
#include "debug/debug_console.h"

#include "mbedtls/net_sockets.h"

void net_sockets_error_info(int ret)
{
	switch (ret){
		case MBEDTLS_ERR_NET_SOCKET_FAILED:
			DEBUGX("MBEDTLS_ERR_NET_SOCKET_FAILED ");
			break;
		case MBEDTLS_ERR_NET_CONNECT_FAILED:
			DEBUGX("MBEDTLS_ERR_NET_CONNECT_FAILED ");
			break;
		case MBEDTLS_ERR_NET_BIND_FAILED:
			DEBUGX("MBEDTLS_ERR_NET_BIND_FAILED ");
			break;
		case MBEDTLS_ERR_NET_LISTEN_FAILED:
			DEBUGX("MBEDTLS_ERR_NET_LISTEN_FAILED ");
			break;
		case MBEDTLS_ERR_NET_ACCEPT_FAILED:
			DEBUGX("MBEDTLS_ERR_NET_ACCEPT_FAILED ");
			break;
		case MBEDTLS_ERR_NET_RECV_FAILED:
			DEBUGX("MBEDTLS_ERR_NET_RECV_FAILED ");
			break;
		case MBEDTLS_ERR_NET_SEND_FAILED:
			DEBUGX("MBEDTLS_ERR_NET_SEND_FAILED ");
			break;
		case MBEDTLS_ERR_NET_CONN_RESET:
			DEBUGX("MBEDTLS_ERR_NET_CONN_RESET ");
			break;
		case MBEDTLS_ERR_NET_UNKNOWN_HOST:
			DEBUGX("MBEDTLS_ERR_NET_UNKNOWN_HOST ");
			break;
		case MBEDTLS_ERR_NET_BUFFER_TOO_SMALL:
			DEBUGX("MBEDTLS_ERR_NET_BUFFER_TOO_SMALL ");
			break;
		case MBEDTLS_ERR_NET_INVALID_CONTEXT:
			DEBUGX("MBEDTLS_ERR_NET_INVALID_CONTEXT ");
			break;
	}
}