/*
  HTTP client using socket 

  HieuNT
*/

/* Includes  -----------------------------------------------------------------*/
#define __DEBUG__ 4
#ifndef __MODULE__
	#define __MODULE__ "httpclient_socket"
#endif
#include "debug/debug.h"
#include "debug/debug_console.h"

#include "lwip/sys.h"
#include "lwip/sockets.h"
#include "lwip/netdb.h"
#include "string.h"
#include "httpclient-socket.h"
#include "cmsis_os.h"
#include <stdio.h>
#include "target.h"

static char data_buffer[80];

#include "lwip/netif.h"
extern struct netif gnetif; 

int http_client_socket_get(const char *host, const char *path)
{
  int socket_fd;
	struct sockaddr_in sa,ra;

	int ret; /* Creates an TCP socket (SOCK_STREAM) with Internet Protocol Family (PF_INET).
	 * Protocol family and Address family related. For example PF_INET Protocol Family and AF_INET family are coupled.
	*/

	socket_fd = socket(PF_INET, SOCK_STREAM, 0);

	if ( socket_fd < 0 )
	{
		DEBUG("socket call failed");
		return(-1);
	}

	// Source address
	memset(&sa, 0, sizeof(struct sockaddr_in));
	sa.sin_family = AF_INET;
	inet_addr_from_ipaddr(&sa.sin_addr, &gnetif.ip_addr);
	sa.sin_port = htons(SENDER_PORT_NUM);


	/* Bind the TCP socket to the port SENDER_PORT_NUM and to the current
	* machines IP address (Its defined by SENDER_IP_ADDR). 
	* Once bind is successful for UDP sockets application can operate
	* on the socket descriptor for sending or receiving data.
	*/
//	if (bind(socket_fd, (struct sockaddr *)&sa, sizeof(struct sockaddr_in)) == -1)
//	{
//		DEBUG("Bind to Port Number %d  for source IP address failed\n", SENDER_PORT_NUM);
//		close(socket_fd);
//		return(-2);
//	}
	/* Receiver connects to server ip-address. */

	memset(&ra, 0, sizeof(struct sockaddr_in));
	ra.sin_family = AF_INET;
	ra.sin_addr.s_addr = inet_addr(SERVER_IP_ADDRESS);
	ra.sin_port = htons(SERVER_PORT_NUM);

	if(connect(socket_fd, (struct sockaddr *)&ra, sizeof(ra)) < 0)
	{
		DEBUG("connect failed \n");
		close(socket_fd);
		return(-3);
	}

	while(1){
		// send
		sprintf(data_buffer, "tick %u\n", millis());
		ret = (int32_t) write(socket_fd, data_buffer, strlen(data_buffer));
		if(ret < 0)
		{
			DEBUG("send failed \n");
			close(socket_fd);
			return(-4);
		}
		else if (ret > 0){
			DEBUG("send success: %d\n", ret);
		}
		// receive
		ret = recv(socket_fd,data_buffer,sizeof(data_buffer), MSG_DONTWAIT);
		if(ret < 0)
		{
			// DEBUG("recv failed: %d\n", ret);
			// close(socket_fd);
			// return(-5);
		}
		else if (ret > 0){
			data_buffer[ret] = '\0';
			DEBUG("received data: %s\n",data_buffer);	
		}
		osDelay(10);
	}

	close(socket_fd);
	return (0);
}

void http_client_test_dns(void)
{
	int ret;
	int fd;
	struct addrinfo hints;
  struct addrinfo *list;
  struct addrinfo *current;

  /* Do name resolution with both IPv6 and IPv4 */
  memset(&hints, 0, sizeof(hints));
  hints.ai_family = AF_UNSPEC;
  hints.ai_socktype = SOCK_STREAM;
  hints.ai_protocol = IPPROTO_TCP;

  if(ret = getaddrinfo(SERVER_NAME, SERVER_PORT, &hints, &list) != 0){
  	DEBUG("getaddrinfo failed: %d\n", ret);
  	return;
  }

  for( current = list; current != NULL; current = current->ai_next)
  {
    fd = (int) socket(current->ai_family, current->ai_socktype, current->ai_protocol);
    if(fd < 0)
    {
      DEBUG("socket failed: %d\n", fd);
      continue;
    }

    if(connect(fd, current->ai_addr, (uint32_t)current->ai_addrlen) == 0)
    {
      DEBUG("Yeah, connect success\n");
      close(fd);
      break;
    }

    close(fd);
  }

  freeaddrinfo(list);

  DEBUG("Exit test dns\n");
}