/*
	Target specific include header file

	HieuNT
	20/3/2017
*/

#ifndef __TARGET_SPEC_H_
#define __TARGET_SPEC_H_

#ifdef __cplusplus
extern "C" {
#endif

// types
#include <string.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>
#include <ctype.h>
#include <stdlib.h>

#include "stm32f2xx_hal.h"
#include "cmsis_os.h"

// HieuNT: add for convenient
#define _sizeof(x) (sizeof(x)-1)
#define _malloc(x) pvPortMalloc(x)
#define _free(x) vPortFree(x)
#define delay(x) osDelay(x)

// HAL specific
uint32_t HAL_GetTick(void);
#define millis() HAL_GetTick()
// void DWT_Delay(uint32_t us);
#define delay_us(us) DWT_Delay(us)

#define __SET_BIT(reg, x) do {reg |= x;} while(0)
#define __CLR_BIT(reg, x) do {reg &= ~(x);} while(0)	

#ifdef __cplusplus
}
#endif

#endif
