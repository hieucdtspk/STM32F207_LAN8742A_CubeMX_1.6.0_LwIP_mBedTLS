/**
	Header file for UART DMA using as ringbuffer implementation!
	
	HieuNT
	17/7/2015
*/

// TODO: remove hdma_uart_rx & hdma_uart_tx in UART_DMA_RINGBUFFER_T
// TODO: since dma already linked in huart

#ifndef __UART_DMA_RINGBUFFER__
#define __UART_DMA_RINGBUFFER__

#ifdef __cplusplus
 extern "C" {
#endif

#include "ringbuf_dma.h"

/* // dma v2
typedef enum {
	UART_DMA_RX_STATE_STOPPED = 0,
	UART_DMA_RX_STATE_RUNNING,
	UART_DMA_RX_STATE_PAUSED
} UART_DMA_RX_STATE_T;
*/

typedef struct {
	UART_HandleTypeDef *huart;
	DMA_HandleTypeDef	*hdma_uart_rx;
	DMA_HandleTypeDef *hdma_uart_tx;
	uint8_t *rxBuffer;
	uint8_t *txBuffer;
	uint32_t rxBufferSize;
	uint32_t txBufferSize;
	RINGBUF rxRingbuffer;
	RINGBUF txRingbuffer;
	__IO int32_t isSending; // should be atomic
	__IO uint32_t currentDMATxSize; 
	//__IO UART_DMA_RX_STATE_T rxDmaState; // should be atomic
	void (*rxInterruptCallback)(void *huart_dma); // can be used for semaphore synchronization
} UART_DMA_RINGBUFFER_T;

void UART_DMA_Init(UART_DMA_RINGBUFFER_T *huart_dma, 
												UART_HandleTypeDef *huart,
												DMA_HandleTypeDef *hdma_uart_rx,
												DMA_HandleTypeDef *hdma_uart_tx, 
												uint8_t *rxBuffer, uint8_t *txBuffer,
												uint32_t rxBufferSize, uint32_t txBufferSize,
												void (*rxInterruptCallback)(void *huart_dma));
void UART_DMA_TX_SendByte(UART_DMA_RINGBUFFER_T *huart_dma, uint8_t c);
void UART_DMA_TX_SendString(UART_DMA_RINGBUFFER_T *huart_dma, uint8_t *s);
void UART_DMA_TX_SendBlock(UART_DMA_RINGBUFFER_T *huart_dma, uint8_t *buffer, uint32_t size);
void UART_DMA_TX_DMAComplete(UART_DMA_RINGBUFFER_T *huart_dma);
void UART_DMA_TX_StartDMA(UART_DMA_RINGBUFFER_T *huart_dma);
void UART_DMA_RX_StartDMA(UART_DMA_RINGBUFFER_T *huart_dma);
//void UART_DMA_RX_PauseDMA(UART_DMA_RINGBUFFER_T *huart_dma);
//void UART_DMA_RX_ResumeDMA(UART_DMA_RINGBUFFER_T *huart_dma);
void UART_DMA_RX_DMAComplete(UART_DMA_RINGBUFFER_T *huart_dma);
uint32_t UART_DMA_RX_Available(UART_DMA_RINGBUFFER_T *huart_dma);
uint32_t UART_DMA_RX_Count(UART_DMA_RINGBUFFER_T *huart_dma);
int32_t UART_DMA_RX_Get(UART_DMA_RINGBUFFER_T *huart_dma, uint8_t *c);
uint32_t UART_DMA_RX_GetBlock(UART_DMA_RINGBUFFER_T *huart_dma, uint8_t *buffer, uint32_t size);
//void UART_DMA_IRQHandler(UART_DMA_RINGBUFFER_T *huart_dma);
void UART_DMA_RX_Flush(UART_DMA_RINGBUFFER_T *huart_dma);

// Link in specific env / or using HAL_Delay if not implemented
void UART_DMA_Delay(uint32_t ms);

#ifdef __cplusplus
 }
#endif

#endif // __UART_DMA_RINGBUFFER__
