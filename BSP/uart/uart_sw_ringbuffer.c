/**
	Source file for UART SW using ringbuffer implementation!
	
	HieuNT
	17/7/2015
*/

#include "target.h"
#include "uart_sw_ringbuffer.h"

__weak void UART_SW_Delay(uint32_t ms)
{
	HAL_Delay(ms);
}

void UART_SW_Init(UART_SW_RINGBUFFER_T *huart_sw, 
												UART_HandleTypeDef *huart,
												uint8_t *rxBuffer, uint8_t *txBuffer,
												uint32_t rxBufferSize, uint32_t txBufferSize,
												void (*rxInterruptCallback)(void *huart_sw))
{
	huart_sw->huart = huart;
	huart_sw->rxBuffer = rxBuffer;
	huart_sw->txBuffer = txBuffer;
	huart_sw->txBufferSize = txBufferSize;
	huart_sw->rxBufferSize = rxBufferSize;
	RINGBUF_Init(&huart_sw->txRingbuffer, huart_sw->txBuffer, huart_sw->txBufferSize);
	RINGBUF_Init(&huart_sw->rxRingbuffer, huart_sw->rxBuffer, huart_sw->rxBufferSize);
	huart_sw->rxInterruptCallback = rxInterruptCallback;
	huart_sw->isSending = 0;
}

void UART_SW_TX_SendByte(UART_SW_RINGBUFFER_T *huart_sw, uint8_t c)
{
	while (!RINGBUF_Put(&huart_sw->txRingbuffer, c)){ // to make sure no character missing
		UART_SW_Delay(5);
	}
	UART_SW_TX_Start(huart_sw);
}

void UART_SW_TX_SendString(UART_SW_RINGBUFFER_T *huart_sw, uint8_t *s)
{
	while(*s){
		while (!RINGBUF_Put(&huart_sw->txRingbuffer, *s)){ // to make sure no character missing
			// dbg("tx overflow\n\r");
			UART_SW_TX_Start(huart_sw);
			UART_SW_Delay(10);
		}
		s++;
	}
	UART_SW_TX_Start(huart_sw);
}

void UART_SW_TX_SendBlock(UART_SW_RINGBUFFER_T *huart_sw, uint8_t *buffer, uint32_t size)
{
	__IO uint32_t remain = size, free, i;
	uint8_t *buf = buffer;
	
	while (remain){
		free = RINGBUF_Free(&huart_sw->txRingbuffer);
		if (free == 0){ // to make sure no data missing
			UART_SW_Delay(10);
			continue; 
		}
		if (free >= remain){
			for (i=0;i<remain;i++){
				RINGBUF_Put(&huart_sw->txRingbuffer, *buf++);
			}
			UART_SW_TX_Start(huart_sw);
			remain = 0;
		}
		else {
			for (i=0;i<free;i++){
				RINGBUF_Put(&huart_sw->txRingbuffer, *buf++);
			}
			UART_SW_TX_Start(huart_sw);
			remain -= free;
		}
	}
}

// This must be called in HAL_UART_TxCpltCallback()
void UART_SW_TX_Complete(UART_SW_RINGBUFFER_T *huart_sw)
{
	huart_sw->isSending = 0;
	if (RINGBUF_Available(&huart_sw->txRingbuffer)){
		UART_SW_TX_Start(huart_sw);
	}
}

// Private
void UART_SW_TX_Start(UART_SW_RINGBUFFER_T *huart_sw)
{
	if (huart_sw->isSending) return;
	
	if (RINGBUF_Available(&huart_sw->txRingbuffer)){
		RINGBUF_Get(&huart_sw->txRingbuffer, &huart_sw->txByte);
		huart_sw->isSending = 1;
		HAL_UART_Transmit_IT(huart_sw->huart, &huart_sw->txByte, 1);
	}
}

// This should be call before any actual rx (one time)
void UART_SW_RX_Start(UART_SW_RINGBUFFER_T *huart_sw)
{
	huart_sw->isReceiving = 1;
	HAL_UART_Receive_IT(huart_sw->huart, &huart_sw->rxByte, 1);
}


// This must be called in HAL_UART_RxCpltCallback()
void UART_SW_RX_Complete(UART_SW_RINGBUFFER_T *huart_sw)
{
	if (RINGBUF_Free(&huart_sw->rxRingbuffer) > 0){
		RINGBUF_Put(&huart_sw->rxRingbuffer, huart_sw->rxByte);
		HAL_UART_Receive_IT(huart_sw->huart, &huart_sw->rxByte, 1);
	}
	else {
		huart_sw->isReceiving = 0;
	}
	if (huart_sw->rxInterruptCallback != NULL) huart_sw->rxInterruptCallback((void *)huart_sw);
}

uint32_t UART_SW_RX_Available(UART_SW_RINGBUFFER_T *huart_sw)
{
	return (uint32_t)RINGBUF_Available(&huart_sw->rxRingbuffer);
}

uint32_t UART_SW_RX_Count(UART_SW_RINGBUFFER_T *huart_sw)
{
	return (uint32_t)RINGBUF_Count(&huart_sw->rxRingbuffer);
}

// return: 0 if no data availalbe
int32_t UART_SW_RX_Get(UART_SW_RINGBUFFER_T *huart_sw, uint8_t *c)
{
	int32_t res = RINGBUF_Get(&huart_sw->rxRingbuffer, c);
	if (huart_sw->isReceiving == 0){
		huart_sw->isReceiving = 1;
		HAL_UART_Receive_IT(huart_sw->huart, &huart_sw->rxByte, 1);
	}
	return res;
}

// Return: size of block has been read (non-blocking function)
uint32_t UART_SW_RX_GetBlock(UART_SW_RINGBUFFER_T *huart_sw, uint8_t *buffer, uint32_t size)
{
	__IO uint32_t remain = size, available, count = 0, i;
	uint8_t *buf = buffer;
	
	while(remain){
		available = RINGBUF_Count(&huart_sw->rxRingbuffer);
		if (available == 0) return count;
		if (remain <= available){
			for (i=0;i<remain;i++){
				RINGBUF_Get(&huart_sw->rxRingbuffer, buf++);
			}
			count += remain;
			remain = 0;
		}
		else {
			for (i=0;i<available;i++){
				RINGBUF_Get(&huart_sw->rxRingbuffer, buf++);
			}
			count += available;
			remain -= available;
		}
		
		if (huart_sw->isReceiving == 0){
			huart_sw->isReceiving = 1;
			HAL_UART_Receive_IT(huart_sw->huart, &huart_sw->rxByte, 1);
		}
	}
	return count;
}

void UART_SW_RX_Flush(UART_SW_RINGBUFFER_T *huart_sw)
{
	RINGBUF_Flush(&huart_sw->rxRingbuffer);
}
